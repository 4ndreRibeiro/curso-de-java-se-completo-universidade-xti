/**
 * Apresentar os conceitos de operadoresmatemáticos.
 * @author andre ribeiro dos santos
 */
public class OperadoresMatematicos {
  public static void main(String[] args) {
    // -2 operador unário
    // 2 + 3 Operador binário
    // true ? "sim" : "não" Opérador ternário

    int x = 7 % 3;
    System.out.println(x);
  }
}