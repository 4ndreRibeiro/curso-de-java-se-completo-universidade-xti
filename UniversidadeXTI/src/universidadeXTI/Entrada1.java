package universidadeXTI;
import javax.swing.*;

/**
 * Apresentar os conceitos de entrada de dados com scanner.
 * @author André Ribeiro
 */

public class Entrada1 {
  public static void main(String[] args) {
    String nome = JOptionPane.showInputDialog("Qual o seu nome");
    System.out.println(nome);
  }
}