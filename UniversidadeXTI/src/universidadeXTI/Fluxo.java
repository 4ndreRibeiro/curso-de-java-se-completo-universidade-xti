/**
 * Trabalhando com Fluxo.
 *
 * @author andre ribeiro dos santos
 */
public class Fluxo {
  public static void main(String[] args) {

    int idade = 36;
    if (idade <= 11) {
      System.out.println("Criança");
    }else if (idade > 11 && idade <= 18) {
      System.out.println("Adolecente");
    } else if (idade > 18 && idade <= 60) {
      System.out.println("Adulto");
    } else {
      System.out.println("Melhor Idade");
    }
  }
}