import java.util.Random;
import java.util.Scanner;

/**
 * Trabalhando com Fluxo com switch.
 * Dsafio do dado.
 * @author andre ribeiro dos santos
 */
public class DesafioDoDado {
  public static void main(String[] args) {
    Scanner s = new Scanner(System.in);
    System.out.println("Qual o seu palpite");
    int palpite = s.nextInt();

    Random n = new Random();
    int dado = n.nextInt(6) + 1; //0-5

    System.out.println("Palpite = " + palpite);
    System.out.println("Dado = " + dado);

    if (palpite == dado) {
      System.out.println("Acertou");
    } else {
      System.out.println("Errou");
    }
  }
}