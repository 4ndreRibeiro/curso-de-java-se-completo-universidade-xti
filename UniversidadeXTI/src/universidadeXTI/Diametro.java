import java.util.Scanner;

/**
 * Apresentar os conceitos para calcular o diametro.
 * @author andre ribeiro dos santos
 */
public class Diametro {
  public static void main(String[] args) {

    Scanner s = new Scanner(System.in);
    System.out.println("Informe o raio");
    double raio = s.nextDouble();
    //Diâmetro : 2r
    //double raio = 10;
    double diametro = 2 * raio;
    System.out.println("Diametro: " + diametro);

    //circunferencia : 2 PI r
    double pi = Math.PI;
    double circunferencia = 2 * pi * raio;
    System.out.println("Circunferencia: " + circunferencia);

    //Área PI r2
    double area = pi * (raio * raio);
    System.out.println("Área:" + area);

  }
}