import java.util.ArrayList;
import java.util.Scanner;
/**
 * Trabalhando com foreach usando while. *
 * @author andre ribeiro dos santos
 */
public class While1 {
  public static void main(String[] args) {
    /* FLUXO DE REPETIÇÃO */
    ArrayList<String> produtos = new ArrayList<String>();
    Scanner s = new Scanner(System.in);
    System.out.println("Liste seu Produtos : Para sair digite FIM");

    String produto;

    while (! "FIM".equals(produto = s.nextLine())) {
      produtos.add(produto);
    }
    System.out.println(produtos.toString());
  }
}
