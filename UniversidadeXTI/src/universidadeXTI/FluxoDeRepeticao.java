/**
 * Trabalhando com foreach usando while. *
 *
 * @author andre ribeiro dos santos
 */
public class FluxoDeRepeticao {
  public static void main(String[] args) {

    /* FLUXO DE REPETIÇÃO */
    for (int i = 0; i < 10; i++) {
      if (i == 5) {
        continue;//pula onde está na posição e continua apartir do proximo numero
      }
      System.out.println(i);

    }
  }
}