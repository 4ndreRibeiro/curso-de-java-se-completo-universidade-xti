/**
 * Apresentar os conceitos de operadores de atribuição.
 * @author andre ribeiro dos santos
 */
public class OperadoresAtribuicao {
  public static void main(String[] args) {
    Integer x = 6;
    x += 3;
    System.out.println(x);
  }
}