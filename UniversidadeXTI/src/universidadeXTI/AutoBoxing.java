/**
 * Trabalhando com break e continue. *
 *
 * @author andre ribeiro dos santos
 */
public class AutoBoxing {
  public static void main(String[] args) {

    Integer x = 555;
    x++; // desempacotando, incrementando e re-empacotando
    System.out.println(x);

    Boolean v = new Boolean("true");
    if(v) {
      System.out.println(v);
    }
  }
}