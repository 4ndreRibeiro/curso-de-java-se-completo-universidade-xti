/**
 * Apresentar os conceitos de tipos e conversão wrapper.
 *
 * @author andre ribeiro dos santos
 */
public class Wrappers {
  public static void main(String[] args) {

    Double preco = new Double("12.45");
    double d = preco.intValue();
    int i = preco.intValue();
    byte b = preco.byteValue();

    //CONVERSÃO ESTATICA
    double d1 = Double.parseDouble("123.45");
    int i1 = Integer.parseInt("123");
    float f1 = Float.parseFloat("3.14F");
    int i2 = Integer.valueOf("101011", 2);

    System.out.println(preco);
    System.out.println(d);
    System.out.println(i);
    System.out.println(b);
    System.out.println(d1);
    System.out.println(i1);
    System.out.println(f1);
    System.out.println(i2);

  }
}