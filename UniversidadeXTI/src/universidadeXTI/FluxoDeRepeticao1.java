/**
 * Trabalhando com break e continue. *
 *
 * @author andre ribeiro dos santos
 */
public class FluxoDeRepeticao1 {
  public static void main(String[] args) {

    /* FLUXO DE REPETIÇÃO */
    boolean[][] matrix = {
            {false, true, false, false, false},
            {false, false, false, false, false}
    };
    busca:
    for (int a = 0; a < matrix.length; a++) {
      System.out.print("A ");
      for (int b = 0; b < matrix[a].length; b++) {
        if (matrix[a][b]) {
          System.out.print("TRUE ");
          break busca;
          //continue busca;
        }
        System.out.print("B ");
      }
    }
  }
}