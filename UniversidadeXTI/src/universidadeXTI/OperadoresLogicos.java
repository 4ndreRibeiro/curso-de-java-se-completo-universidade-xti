/**
 * Apresentar os conceitos de operadores lógicos.
 * @author andre ribeiro dos santos
 */
public class OperadoresLogicos {
  public static void main(String[] args) {
    Integer x = 6;
    System.out.println(!(x >= 1) && (x <= 10));
  }
}