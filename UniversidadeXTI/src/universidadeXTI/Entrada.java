import java.util.Scanner;

/**
 * Apresentar os conceitos de entrada de dados com scanner.
 * @author andre ribeiro dos santos
 */
public class Entrada {
  public static void main(String[] args) {

    //Interagindo com o Usuário
    Scanner s = new Scanner(System.in);
    System.out.println("Qual o seu nome?");
    String nome = s.nextLine();
    System.out.println("Bem vindo " + nome);

  }
}