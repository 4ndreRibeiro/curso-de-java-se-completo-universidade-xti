/**
 * Trabalhando com Loop.
 * Dsafio do dado.
 *
 * @author andre ribeiro dos santos
 */
public class Loops {
  public static void main(String[] args) {
    String texto = "";
    for (int i = 0; i <= 20; i++) {
      if (i % 2 == 0) {
        texto += i + ",";
      }
    }
    System.out.println(texto);
  }
}