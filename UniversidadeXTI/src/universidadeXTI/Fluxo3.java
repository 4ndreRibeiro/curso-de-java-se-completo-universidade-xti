/**
 * Trabalhando com Fluxo com switch.
 *
 * @author andre ribeiro dos santos
 */
public class Fluxo3 {
  public static void main(String[] args) {
    String tecnologia = "postgresql";
    switch (tecnologia) {
      case "java":
      case "c++":
      case "cobol":
        System.out.println("Linguagem de programação");
        break;
      case "oracle":
      case "sqlserver":
      case "postgresql":
        System.out.println("Banco de dados");
        break;
      default:
        System.out.println("Tecnologia desconhecida");

    }
  }
}