/**
 * DESAFIO : Fibonacci
 * Começa-se a séria com 0 (zero) e 1 (um)
 * Obtém-se o próximo número de fibonacci
 * somando-se os dois anteriores e, assim,
 * sucessivamente e infinitivamente.
 *
 * Ex : 1+2[3] 2+3[5] 3+5[8] 5+8[13] ...
 *
 * 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89,
 * 144, 233, 377, 610, 987, 1597, 2584, ...
 *
 * Trabalhando com foreach usando while. *
 * @author andre ribeiro dos santos
 */

public class Fibonacci {
  public static void main(String[] args) {
    /* FLUXO DE REPETIÇÃO */
    int anterior = 0;
    int proximo = 1;
    System.out.println(anterior);

    while (proximo < 999999999) {
      System.out.println(proximo);
      proximo = proximo + anterior; // Próximo número fibonacci
      anterior = proximo - anterior; // Atualizando o número anterior (soma - anterior)
    }
  }
}