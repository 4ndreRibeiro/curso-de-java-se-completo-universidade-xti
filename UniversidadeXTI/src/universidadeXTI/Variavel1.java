public class Variavel1 {
  /**
   * Apresentar os conceitos de Variável.
   * @author andre ribeiro dos santos
   */
    public static void main(String[] args) {

      int populacaoBrasileira = 203429773;
      populacaoBrasileira = 123;

      final  double PI = 3.14159265359879323846;
      final char SEXO_MASCULINO = 'M';
      final char SEXO_FEMININO = 'F';

      System.out.println(populacaoBrasileira);
      System.out.println(PI);
      System.out.println(SEXO_MASCULINO);
      System.out.println(SEXO_FEMININO);

  }
}