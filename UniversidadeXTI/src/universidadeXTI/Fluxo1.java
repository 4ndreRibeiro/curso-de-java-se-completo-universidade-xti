/**
 * Trabalhando com Fluxo.
 *
 * @author andre ribeiro dos santos
 */
public class Fluxo1 {
  public static void main(String[] args) {

    int nota = 4;
    if (nota >= 7) {
      System.out.println("Passou");
    } else {
      System.out.println("Reprovou");
      if (nota >= 6) {
        System.out.println("Mais pode fazer a recuperação");
      }
    }
  }
}