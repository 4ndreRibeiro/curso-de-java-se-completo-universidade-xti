/**
 * Apresentar os conceitos de Variável.
 * @author andre ribeiro dos santos
 */
public class Variavel {
  public static void main(String[] args) {
    String nome = "André Ribeiro";
    int idade = 36;
    boolean casado = true;

    byte b = 127; //cem
    short s = 32767; //32 mil
    int i = 2000000000; //2 bilhões
    long l = 9000000000000000000L; //9 quintilhões
    double d = 1.7976931348623157E+308; //IEEE 754
    float f = 123F;

    byte bb = 0b01010101; //8 bits/ 1 byte
    short ss = 0b0101010101010101; //16 bits/ 2 bytes
    int ii = 0b01010101010101010101010101010101; //32 bits/ 4 bytes
    System.out.println("Texto");
    System.out.println("Seu nome eh: " + nome);
    System.out.println("Sua Idade eh: " + idade);
    System.out.println(b);
    System.out.println(s);
    System.out.println(i);
    System.out.println(d);
    System.out.println(f);
    System.out.println(bb);
    System.out.println(ss);
    System.out.println(ii);

  }
}