import javax.swing.*;

/**
 * Calcular o indice de massa corporal IMC.
 * IMC = pesoEmQuilogramas / (alturaEmMetros * alturaEmMetros);
 *
 * @author andre ribeiro dos santos
 */
public class IMC {
  public static void main(String[] args) {
    String peso = JOptionPane.showInputDialog("Qual seu peso em quilograma?");
    String altura = JOptionPane.showInputDialog("Qual e a sua altura");

    double pesoEmQuilogramas = Double.parseDouble(peso);
    double alturaEmMetros = Double.parseDouble(altura);
    double imc = pesoEmQuilogramas / (alturaEmMetros * alturaEmMetros);

    String msg = (imc >= 20 && imc <= 25 ) ? "Peso Ideal" : "Fora do Peso Ideal";
    msg = "IMC = " + imc + "\n" + msg;

    JOptionPane.showMessageDialog(null, msg);
    System.out.println("IMC" + imc);
    System.out.println(msg);
  }
}