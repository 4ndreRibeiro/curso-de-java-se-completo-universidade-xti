/**
 * Trabalhando com Fluxo com switch.
 *
 * @author andre ribeiro dos santos
 */
public class Fluxo2 {
  public static void main(String[] args) {
    char sexo = 'S';
    switch (sexo) {
      case 'M':
        System.out.println("Macho");
        break;
      case 'F':
        System.out.println("Femea");
        break;
      default:
        System.out.println("Outro");

    }
  }
}