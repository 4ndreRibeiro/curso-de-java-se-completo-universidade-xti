import java.util.Arrays;

/**
 * Trabalhando com Arrays Composto.
 * @author andre ribeiro dos santos
 */
public class ArrayComposto {
  public static void main(String[] args){

    String[] [] duas = {{"André", "M", "SP"}, {"Larissa", "F", "PR"}};
    System.out.println(duas[0][0]);
    System.out.println(duas[0].length);

  }
}