import java.util.ArrayList;

/**
 * Trabalhando com Arrays Composto.
 *
 * @author andre ribeiro dos santos
 */
public class ArrayListe {
  public static void main(String[] args) {

    ArrayList<String> cores = new ArrayList<>();
    cores.add("Branco");
    cores.add(0, "Vermelho");
    cores.add("Amarelo");
    cores.add("Azul");

    System.out.println(cores.toString());

    System.out.println("Tamanho= " + cores.size());
    System.out.println("Elemento= " + cores.get(2));

    cores.remove("Branco");

    System.out.println("Tem Cinza ? " + cores.contains("Cinza"));

  }
}