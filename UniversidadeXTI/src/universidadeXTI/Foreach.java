/**
 * Trabalhando com foreach.
 * Dsafio do dado.
 *
 * @author andre ribeiro dos santos
 */
public class Foreach {
  public static void main(String[] args) {

    int[] pares = {2,4,6,8};
      for (int i = 0; i < pares.length; i++) {
        int par = pares[i];
        System.out.print(par);
      }
      for (int par : pares) {
      System.out.println(par);
    }
  }
}