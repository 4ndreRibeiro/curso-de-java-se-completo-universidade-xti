import java.util.ArrayList;

/**
 * Trabalhando com foreach usando ArrayList. *
 * @author andre ribeiro dos santos
 */
public class Foreach1 {
  public static void main(String[] args) {

    ArrayList<Integer> list = new ArrayList<Integer>();
    for (int i = 0; i < 10; i++) {
      list.add(i);
    }
    for (Integer numero : list) {
      System.out.println(numero);
    }
  }
}