/**
 *  Programa que imprime texto na tela.
 * @author andre ribeiro dos santos
 */

public class HelloWorld {

    public static void main(String[] args){

        System.out.println("Hello World");
        System.out.println("Java SE");
    }   
}