/**
 * Apresentar os conceitos de operadores especiais.
 * @author andre ribeiro dos santos
 */
public class OperadoresEspeciais {
  public static void main(String[] args) {
    int idade = 36;
    String x = (idade >= 18) ? "Maior de Idade" : "Menor de Idade";
    System.out.println(x);
  }
}