/**
 * Trabalhando com foreach usando while. *
 * @author andre ribeiro dos santos
 */
public class While {
  public static void main(String[] args) {

    // FLUXO DE REPETIÇÃO
    int i = 0;
    while (i < 5) {
      System.out.println(i);
      i++;
    };
    do {
      System.out.println(i);
      i++;
    } while (i < 7);
  }
}